jQuery.browser = {};
(function () {
  jQuery.browser.msie = false;
  jQuery.browser.version = 0;
  if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
    jQuery.browser.msie = true;
    jQuery.browser.version = RegExp.$1;
  }
})();

function uploadFile(elem) {
  var filename = (window.FileReader) ? elem.files[0].name : $(this).val().split('/').pop().split('\\').pop();
  var parent = $(elem).closest('.upload');
  
  parent.find('input[type=text]').val(filename);
  if (window.FileReader) {
    parent.children('.upload-display').remove();
    //image 파일만
    if (!$(elem)[0].files[0].type.match(/image\//)) return;

    var reader = new FileReader();
    reader.onload = function(e) {
      var src = e.target.result;
      parent.prepend('<div class="upload-display"><img src="' + src + '" class="upload-thumb js-toggle-img" onclick="uploadThumb(this)"></div>');
    }
    reader.readAsDataURL($(elem)[0].files[0]);
  }
}

function uploadThumb(elem) { 
  $(elem).toggleClass('lg');
} 

function uploadDel(elem) {
  var p = $(elem).closest('.upload-wrap').find('.upload').length;
  var file = $(elem).closest('.upload').find('input[type=file]');
  var text = $(elem).closest('.upload').find('input[type=text]');
  if (p == 1) {
    $(text).val(""); 
    $(elem).closest('.upload').children('.upload-display').remove();
    if ($.browser.msie) { 
      $(file).replaceWith( $(file).clone(true) ); 
    } else { 
      $(file).val(""); 
    }
  } else {
    $(elem).closest('.upload').remove();
  }
}

function uploadAdd(elem) {
  $(elem).prev('.upload-wrap').append(''+
  '<div class="upload">'+
    '<div class="upload__inner">'+
      '<input type="file" name="" id="" class="upload__file" onchange="uploadFile(this)">'+
      '<div class="input-group">'+
        '<input type="text" name="" id="" class="form-control" readonly="readonly">'+
        '<span class="input-group-btn">'+
          '<span class="btn btn-flat btn-default"><i class="fa fa-folder-open-o" aria-hidden="true"></i> 파일선택</span>'+
        '</span>'+
      '</div>'+
    '</div>'+
    '<div class="upload__del">'+
      '<button type="button" class="btn btn-link" onclick="uploadDel(this)"><span class="color-danger"><i class="fa fa-times" aria-hidden="true"></i></span></button>'+
    '</div>'+
  '</div>');
}
$.fn.datetimepicker.dates['ko'] = {
    days: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"], 
    daysShort: ["일", "월", "화", "수", "목", "금", "토"], 
    daysMin: ["일", "월", "화", "수", "목", "금", "토"], 
    months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"], 
    monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    meridiem: ["am", "pm"],
    format: "yyyy-mm-dd",
    titleFormat: "yyyy MM"
};
// $.fn.datepicker.dates['ko'] = {
//     days: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"], 
//     daysShort: ["일", "월", "화", "수", "목", "금", "토"], 
//     daysMin: ["일", "월", "화", "수", "목", "금", "토"], 
//     months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"], 
//     monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
//     format: "yyyy-mm-dd",
//     titleFormat: "yyyy MM"
// };
// $(function(){
// 
//   $('.date').each(function(){
//     var datepickerObj = {
//       language: 'ko',
//       orientation: 'bottom auto'
//     }
//     if($(this).data('autoclose')) {
//       datepickerObj['autoclose'] = true
//     }
//     $(this).datepicker(datepickerObj);
//   })
// })
$(function(){
  $(".datetime").datetimepicker({
      autoclose: !0,
      language: 'ko',
      pickerPosition: 'bottom-start'
  })
  $(".datetime-en").datetimepicker({
      autoclose: !0,
      pickerPosition: 'bottom-start'
  })
});

function imageLg(elem) { 
  $(elem).toggleClass('lg');
} 

//lazy img 
$(function(){
  $('.js-lazy-img').each(function(){
    var $this = $(this);
    if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
      var src = $this.attr('data-src');
      $this.attr('src', src);
    } else {
      var n = function() {
        if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
          var src = $this.attr('data-src');
          $this.attr('src', src);
          $(window).unbind('scroll', n);
        }
      };
      $(window).bind('scroll', n);
    }
  });
});

$(function(){
  $(document).on('click','.js-toggle-img',function(){
    $(this).toggleClass('is-lg');
  });
})